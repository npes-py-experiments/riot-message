# riot message

Purpose of this project:

Connect to matrix.org riot room and send a message from a raspberry pi.
The message contains `datetime` `lan ip address` `wan ip address` `hostname` `SSID`

This is to make it easier to obtain the connection details for a headless RPi

Developed on a RPi 4 with raspbian buster, tested on a RPi 3 with rasbian buster

It's assumed that you know how to set up riot instant messenger  

**usage:**

1. setup [riot.im](https://riot.im) account for the Pi
3. setup riot room, add personal and Pi riot accounts to the room (personal so you receive messages sent from the Pi)
4. clone or fork this repo
5. setup virtual environment on Pi and install required pip packages

```
virtualenv -p python3 venv
source env/bin/activate
pip3 install -r requirements.txt
```
6. create riot_password.txt and put your riot password in the file
7. Edit paths in .py and .sh script to match your user
8. test with `python3 riot_message.py` confirm that you received a message in riot containing ip, ssid etc.
9. `deactivate` virtualenv and test with `./ip_detect.sh`, confirm received riot message 
10. setup a crontab using ´crontab -e´ to execute sh script @reboot  
    ie. `@reboot /home/user/Documents/riot-message/ip_detect.sh &`  
    more in this [https://www.dexterindustries.com/howto/auto-run-python-programs-on-the-raspberry-pi/](https://www.dexterindustries.com/howto/auto-run-python-programs-on-the-raspberry-pi/)
11. reboot the pi and confirm that you get a message on boot

**notes**  
Your user might need to have admin rights, at least the same rights as the default pi user, i think?  
You might consider adding your phones hotspot to the wifi list, more here [https://raspberrypi.stackexchange.com/questions/11631/how-to-setup-multiple-wifi-networks](https://raspberrypi.stackexchange.com/questions/11631/how-to-setup-multiple-wifi-networks)