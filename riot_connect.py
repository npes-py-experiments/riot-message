
#!/usr/bin/python3

import os
import subprocess #used for ssid
from datetime import datetime
from getpass import getpass #password prompt

#matrix api - remember to install it
from matrix_client.client import MatrixClient

# for get ip and hostname
import socket

test_ip = "8.8.8.8"

#get current internal ip and hostname
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect((test_ip, 0))
ipaddr = s.getsockname()[0]
host=socket.gethostname()
current_ip = "IP: " + ipaddr + " Host: " + host
print(current_ip)

#get external ip
externalIP = os.popen('curl -s ifconfig.me').readline()
print(externalIP)

# get ssid
arg_list = [ '/sbin/iwgetid', '-r' ]
args = ' '.join(arg_list)
proc = subprocess.Popen(arg_list, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)
(ssid, dummy) = proc.communicate()
print("SSID: " + ssid)

# Get and format current datetime
now = datetime.now()
dt_string = now.strftime("%Y/%m/%d %H:%M:%S")

# passw = getpass() #password prompt if running script manually
# riot_password.txt is not included in the remote repo for obvious reasons,
# add the file if you are reusing this code
with open("/home/nin/Documents/riot_msg/riot_password.txt") as file:
	passw = file.readline().strip()

# message = input("Write your message: \n") #can be used in room.send_text()

client = MatrixClient("https://matrix.org")

client.login_with_password(username="@ninpi:matrix.org", password=passw)

room = client.join_room("#ninpi:matrix.org")

response = room.send_text(dt_string + " " + host + " is up @ IP " + ipaddr + " SSID: " + ssid + "external IP: " + externalIP)
