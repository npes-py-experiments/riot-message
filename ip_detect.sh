#!/usr/bin/env bash

 set -e

 while ! ping -c 1 -W 1 8.8.8.8; do
	echo "Waiting for 8.8.8.8 - network interface might be down..."
	sleep 1
 done

echo "IPv4 is up"

source /home/nin/Documents/riot_msg/env/bin/activate

python3 /home/nin/Documents/riot_msg/riot_connect.py
