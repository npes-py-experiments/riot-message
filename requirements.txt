certifi==2019.9.11
chardet==3.0.4
idna==2.8
matrix-client==0.3.2
requests==2.22.0
urllib3==1.25.3
